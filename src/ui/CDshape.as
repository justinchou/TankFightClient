package ui
{
	import laya.display.Sprite;
	import laya.display.Text;
	
	import util.Util;
	
	public class CDshape extends Sprite
	{
		public var cdTime:Number;
		private var times:Number;
		private var currentTimes:Number=0;
		private var txt:Text;
		public function CDshape()
		{
			super();
		}
		/**
		 *创建CD计时器 
		 * @param _CDobj  cd需要遮罩的对象
		 * 
		 */		
		public function createCD(_CDobj:Sprite):void{
			this.graphics.drawPie(_CDobj.width/2,_CDobj.height/2,_CDobj.width/2,-90,270,"#000000");
			times=cdTime/10;
			Laya.timer.loop(10,this,cdLine,[_CDobj]);
			this.alpha=.6;
			txt=new Text();
			txt.text=cdTime/1000+"s";
			txt.color="#ff0000";
			txt.fontSize=40;
			txt.size(_CDobj.width,_CDobj.height);
			txt.align="center";
			txt.valign="middle";
			this.addChild(txt);
		}
		
		private function cdLine(_CDobj:Sprite):void
		{
			// TODO Auto Generated method stub
			currentTimes++;
			this.graphics.clear();
			txt.text=Math.floor((cdTime-currentTimes*10)/100)/10+"s";
			this.addChild(txt);
			this.graphics.drawPie(_CDobj.width/2,_CDobj.height/2,_CDobj.width/2,-90+360/times*currentTimes,270,"#000000");
			if(currentTimes>=times){
				clearCd();
				Util.cleanGrayingApe(_CDobj);
				_CDobj.mouseEnabled=true;
			}
		}
		private function clearCd():void{
			Laya.timer.clearAll(this);
			if(this.parent){
				this.parent.removeChild(this);
				this.destroy();
			}
		}
	}
}