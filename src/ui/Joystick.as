package ui
{
	import data.NumericalValueData;
	import data.SocketClient;
	import data.SocketData;
	
	import event.GameEvent;
	
	import laya.display.Sprite;
	import laya.events.Event;
	import laya.maths.Point;
	
	public class Joystick extends Sprite
	{
		private var bg:Sprite;
		public var img:Sprite;
		public var lastData:Object;
		public var flag:Boolean=false;

		public  var curData:Object;
		public function Joystick()
		{
			bg=new Sprite();
			img=new Sprite();
			bg.loadImage("res/game/joystickBg.png");
			img.loadImage("res/game/joystick.png"); 
			this.width=this.height=bg.width;
			this.addChild(bg);
			this.addChild(img);
			bg.pivotX=bg.pivotY=bg.width/2;
			img.pivotY=img.pivotX=img.width/2; 
//			this.visible=false;
//			this.hitArea=new Rectangle(bg.width/2*-1,bg.height/2*-1,bg.width,bg.height);
			this.on(Event.MOUSE_DOWN,this,onDown);  
			
			this.mouseEnabled=true;
			lastData={degrees:0,proportionX:0,proportionY:0};
			curData={degrees:0,proportionX:0,proportionY:0};
		}      
		
		private function onUp(evt:Event):void
		{
			// TODO Auto Generated method stub
//			this.visible=false;
//			if(Laya.stage.mouseX<=320){
				img.pos(0,0);
				evt.stopPropagation();
				Laya.stage.off(Event.MOUSE_MOVE,this,onMove);
				Laya.stage.off(Event.MOUSE_UP,this,onUp);
				SocketData.getInstance().radian=0;
//				SocketData.getInstance().rotation=0;
				SocketData.getInstance().x=0;
				SocketData.getInstance().y=0;
				SocketClient.getInstance().sendData(NumericalValueData.getInstance().apiConfig["ON_SET_TARGET"]);
				event(GameEvent.PLAYER_STOP,{degrees:0,proportionX:0,proportionY:0});
//			}
		
		}
		
		private function onDown(evt:Event):void
		{ 
			// TODO Auto Generated method stub
			evt.stopPropagation();
			Laya.stage.on(Event.MOUSE_MOVE,this,onMove);
			Laya.stage.on(Event.MOUSE_UP,this,onUp);
		}
		
		private function onMove(e:Event):void
		{
			// TODO Auto Generated method stub
//			

			
//			}
			e.stopPropagation();
			var   np:Point=new Point(this.mouseX,this.mouseY);
			var s:Number = Math.sqrt(Math.pow(np.x,2) + Math.pow(np.y,2));
			var r:Number=(bg.width-img.width)/2;
			img.x = s>r?(np.x/s*r):np.x;
			img.y = s>r?(np.y/s*r):np.y;
			var radian:Number = Math.floor(Math.atan2((img.y),(img.x))*10000)/10000;
			var degrees:Number=Math.floor(radian*180/Math.PI);
//			img.x = s>r?(np.x/s*r):r*Math.cos(radian);
//			img.y = s>r?(np.y/s*r):r*Math.sin(radian);
			var proportionX:Number=Math.floor((r*Math.cos(radian)/r)*10000)/10000;
			var proportionY:Number=Math.floor((r*Math.sin(radian)/r)*10000)/10000; 
//			lastData={degrees:degrees,proportionX:Math.floor(proportionX*10000)/10000,proportionY:Math.floor(proportionY*10000)/10000};
//			trace(Math.floor(proportionX*10000)/10000,)
//			var data:Object={degrees:degrees,proportionX:proportionX,proportionY:proportionY};
			curData={
				degrees:degrees,
				proportionX:Math.floor(proportionX*10000)/10000,
				proportionY:Math.floor(proportionY*10000)/10000
			};
			
			if(curData["degrees"]!=lastData["degrees"]
				||curData["proportionX"]!=lastData["proportionX"]
				||curData["proportionY"]!=lastData["proportionY"]
			){
				SocketData.getInstance().packetId=NumericalValueData.getInstance().apiConfig["ON_SET_TARGET"];
				SocketData.getInstance().radian=radian;
				SocketData.getInstance().rotation=curData["degrees"];
				SocketData.getInstance().x=curData["proportionX"];
				SocketData.getInstance().y=curData["proportionY"];
//				trace(JSON.stringify(SocketData.getInstance()));
//				SocketData.getInstance().x=1;
//				SocketData.getInstance().y=0;
//				flag=true;
//				SocketClient.getInstance().sendData(SocketData.getInstance().packetId);
			}
			event(GameEvent.PLAYER_MOVE,curData);
		}
	}
}