package game
{
	import data.NumericalValueData;
	import data.SocketClient;
	import data.SocketData;
	
	import event.GameEvent;
	
	import laya.d3.math.Vector2;
	import laya.display.Sprite;
	import laya.display.Text;
	import laya.events.Event;
	import laya.maths.Point;
	import laya.maths.Rectangle;
	import laya.utils.Handler;
	import laya.utils.Pool;
	import laya.utils.Tween;
	
	import ui.CDshape;
	import ui.Joystick;
	
	import util.Util;
	
	public class Scene extends Sprite
	{
		//操纵杆
		private var joystick:Joystick;
		private var player:BasicPlayer;
		//玩家对象池
		private var playerList:Array;
		//子弹对象池
		private var bulletList:Array=[];
		private var timeIndex:Number=0;
		//用户数量最大值
		private var playerNum:Number=50;
		//技能施法范围指示器
		private var manualControlMask:Sprite;
		//爆炸范围指示器
		private var boomArea:Sprite;
		
		//指示器容器
		 
		//地图底
		private var mapbg:MapPannel;
		
		//ping 显示
		private var pingTxt:Text;
		
		public function Scene()
		{
			super();
			mapbg=new MapPannel();
			addChild(mapbg);
			mapbg.x-=10;
			EffManager.getInstance();
			initBullets()
			initPlayer();
			initProp();
			pingTxt=new Text();
			pingTxt.color="#ffffff";
			pingTxt.fontSize=30;
			pingTxt.x=900;
			Main.uiPannel.addChild(pingTxt);
//			this.cacheAs="bitmap";
//			Laya.timer.loop(10,this,initBullets);
		}
		
		private function initBullets():void
		{
			// TODO Auto Generated method stub
			bulletList=[];
//			for(var i=0;i<1000;i++){
//				var bullet:Bullet=new Bullet();
//				bulletList.push(bullet);
//			}
			connectSocket();
//			if(bullet._style==null)trace(bulletList.length)
			
//			if(bulletList.length>=1000){
//				Laya.timer.clear(this,initBullets);
//				
//			}
		}
		
		private function initProp():void
		{
			// TODO Auto Generated method stub
			manualControlMask=new Sprite();
			manualControlMask.loadImage("res/game/releaseArea1.png");
			manualControlMask.pivot(manualControlMask.width/2,manualControlMask.height/2);
			boomArea=new Sprite();
			boomArea.loadImage("res/game/boomArea.png");
			boomArea.pivot(boomArea.width/2,boomArea.height/2);
		}
		private function initPlayer():void{
			playerList=[];
			for(var i=0;i<playerNum;i++){
				var temP:BasicPlayer=new BasicPlayer();
				playerList.push(temP);
			}
		}
		private function connectSocket():void{
			SocketClient.getInstance().connectSocket();
			SocketClient.getInstance().on(GameEvent.UPDATE_MAP,this,createElement);
			SocketClient.getInstance().on(GameEvent.TO_ADD_PLAYER,this,addPlayer);
		}
		private function addPlayer():void{
			player=new BasicPlayer(2);
			player.pos(SocketData.getInstance().player.position.x,SocketData.getInstance().player.position.y);
			player.objectId=SocketData.getInstance().player.objectId;
			player.on(GameEvent.GUN_BASE_ROTION_COMPLATE,this,rotationComplate);
//			trace(SocketData.getInstance().player.hp);
//			player.HP=SocketData.getInstance().player.hp;
			this.addChild(player)
		
			createScene();
			Laya.timer.loop(50,this,cheakHit);
			
		}
		private function rotationComplate(e:Event):void{
			var target:BasicPlayer=e.currentTarget as BasicPlayer;
			this.addChild(target.tagetBullet);
		}
		private function moveMap():void
		{ 
			if(!player)return;
			this.pos((player.x-Laya.stage.width/2)*-1,(player.y-Laya.stage.height/2)*-1);
			Main.effPannel.pos((player.x-Laya.stage.width/2)*-1,(player.y-Laya.stage.height/2)*-1);
//			trace("玩家坐标："+player.x+","+player.y,"地图坐标："+this.x+","+this.y);
		}
		public function moveHandle(evt:Event):void{  
			player._speedX=Number(SocketData.getInstance().x)*player.maxSpeedX;
			player._speedY=Number(SocketData.getInstance().y)*player.maxSpeedY;	
			player.tankBase.rotation=Number(SocketData.getInstance().rotation);
		} 
		private function createBullet():void{
			ClearNoExistBullet();
//			trace(player.x,player.y);
//			trace(JSON.stringify(SocketData.getInstance().bulletPhoto));
//			trace("bulletPhoto:"+SocketData.getInstance().bulletPhoto.length)
			for each(var bullet:Object in SocketData.getInstance().bulletPhoto){
//				trace(ExistsBullet(bullet["objectId"]))
//				trace("pool:"+bulletList.length)
				var tmpBullet:Number=ExistsBullet(bullet["objectId"]);
//				trace(tmpBullet)
//				if(tmpBullet<0)return;
				if(tmpBullet<0){
//					var tmpBullet:Bullet=Pool.getItemByClass("bullet",Bullet);
					var newBullet:Bullet=Pool.getItemByClass("bullet",Bullet);
//					
					newBullet.objectId=bullet["objectId"];
					newBullet.staticId=bullet["staticId"];
					
					newBullet.x=bullet["position"]["x"];
					newBullet.y=bullet["position"]["y"];
					newBullet.targetPoint.x=bullet["target"]["x"];
					newBullet.targetPoint.y=bullet["target"]["y"];
					newBullet.origin.x=bullet["source"]["x"];
					newBullet.origin.y=bullet["source"]["y"];
//					var angel:Number=
					var radian:Number = Math.floor(Math.atan2((newBullet.targetPoint.y-newBullet.origin.y),(newBullet.targetPoint.x-newBullet.origin.x))*10000)/10000;
					var degrees:Number=Math.floor(radian*180/Math.PI);
					newBullet.rotation=degrees;
					if(bullet["playerId"]==player.objectId){
//						player.gunBase.rotation=degrees;
						player.whirlGun(degrees);
						player.tagetBullet=bulletList[tmpBullet];
					}else{
						var palyerIndex:Number=ExistsPlayer(bullet["playerId"]);
						var tempPlayer:BasicPlayer=playerList[palyerIndex];
						if(newBullet.objectId!=0){
							tempPlayer.whirlGun(degrees);
							tempPlayer.tagetBullet=bulletList[tmpBullet];
						}
					}
					newBullet.initData();
//					tmpBullet.emission();
					this.addChild(newBullet);
					bulletList.push(newBullet);
				}
					else{
					bulletList[tmpBullet].x=bullet["position"]["x"];
					bulletList[tmpBullet].y=bullet["position"]["y"];
				}
//				var tmpBullet:Number = ExistsBullet(Number(bullet["objectId"]));
////				//				trace("for enemy:" + enemy["objectId"]);
//////				if(tmpBullet<0)return;
//////				TracePlayerList();
//				if (bulletList[tmpBullet].objectId == 0){
//					bulletList[tmpBullet].objectId=bullet["objectId"];
//					bulletList[tmpBullet].staticId=bullet["staticId"];
//					bulletList[tmpBullet].x=bullet["position"]["x"]; 
//					bulletList[tmpBullet].y=bullet["position"]["y"];
//					bulletList[tmpBullet].targetPoint.x=bullet["target"]["x"];
//					bulletList[tmpBullet].targetPoint.y=bullet["target"]["y"];
//					bulletList[tmpBullet].origin.x=bullet["source"]["x"];
//					bulletList[tmpBullet].origin.y=bullet["source"]["y"];
//					bulletList[tmpBullet].initData();
//					bulletList[tmpBullet].emission();
//				}
//				this.addChild(bulletList[tmpBullet]);
			}
		}
		/**
		 *创建玩家 
		 * 
		 */		
		private function createElement():void{
			createPlayer();
			createBullet();
			SocketData.getInstance().isProcessorForEnemy=true;

		}

		private function createPlayer():void{
//			trace("服务器快照"+JSON.stringify(SocketData.getInstance().enemyPhoto));
			ClearNoExistEnemy();
			/// new code begin
			player.HP=SocketData.getInstance().player.hp;
			player.maxHp=SocketData.getInstance().player.maxHp;
			player.upDataBlood();
//			trace(SocketData.getInstance().player.hp)
			for each(var enemy:Object in SocketData.getInstance().enemyPhoto){
				var tmpPlayer:Number = ExistsPlayer(Number(enemy["objectId"]));
//				trace("for enemy:" + enemy["objectId"]);
				if(tmpPlayer<0)return;
//				TracePlayerList();
				if (playerList[tmpPlayer].objectId == 0){
					playerList[tmpPlayer].objectId=enemy["objectId"];
					playerList[tmpPlayer].x=enemy["position"]["x"]; 
					playerList[tmpPlayer].y=enemy["position"]["y"];
//					playerList[tmpPlayer].HP=enemy["hp"];
				}
				playerList[tmpPlayer].tankBase.rotation=Number(enemy["rotation"]);
				playerList[tmpPlayer].x=enemy["position"]["x"]; 
				playerList[tmpPlayer].y=enemy["position"]["y"];
//				playerList[tmpPlayer]._speedX=enemy["proportion"]["x"]*playerList[tmpPlayer].maxSpeedX;
//				playerList[tmpPlayer]._speedY=enemy["proportion"]["y"]*playerList[tmpPlayer].maxSpeedY;
				playerList[tmpPlayer].HP=enemy["hp"];
				playerList[tmpPlayer].maxHp=enemy["maxHp"];
				playerList[tmpPlayer].upDataBlood();
				playerList[tmpPlayer].on(GameEvent.GUN_BASE_ROTION_COMPLATE,this,rotationComplate)
				this.addChild(playerList[tmpPlayer]);
			}
		}

		private function softList(a:Object,b:Object):Number{
			return a["objectId"]-b["objectId"];
		}
		private function addAndRemove(np:Object,p:BasicPlayer,i1:Number,i2:Number):Boolean{
//			trace(p.type,i1,i2);
			return false;
		}
		
		private function createScene():void
		{
			// TODO Auto Generated method stub
			joystick=new Joystick();
			joystick.pos(joystick.width/2+joystick.width/2,Laya.stage.height-(joystick.height));
//			joystick.pos(0,0)
			Main.uiPannel.addChild(joystick);
			joystick.on(GameEvent.PLAYER_MOVE,this,moveHandle);
			var rectangle:Rectangle=new Rectangle(-10,-10,joystick.width+20,joystick.height+20);
			joystick.hitArea=rectangle;
			joystick.on(GameEvent.PLAYER_STOP,player,player.stand);
			 
			
			var actBtn1:Sprite=new Sprite();
			actBtn1.name="10000204";
			actBtn1.loadImage("res/game/bulletBtn.png");
			actBtn1.pos(Laya.stage.width-actBtn1.width-15,Laya.stage.height-actBtn1.height-actBtn1.height/2);
			Main.uiPannel.addChild(actBtn1);
			actBtn1.on(Event.CLICK,this,clickHandle);
			
			var actBtn2:Sprite=new Sprite();
			actBtn2.name="10000202";
			actBtn2.loadImage("res/game/excuseBtn.png");
			actBtn2.pos((actBtn1.x-actBtn2.width-15),actBtn1.y+actBtn2.height/2);
			Main.uiPannel.addChild(actBtn2);
			actBtn2.on(Event.CLICK,this,clickHandle);
			
			var actBtn3:Sprite=new Sprite();
			actBtn3.name="10000201";
			actBtn3.loadImage("res/game/primerBtn.png");
			actBtn3.pos(actBtn1.x-actBtn3.width/2-15,actBtn1.y-actBtn3.height/2-15);
			Main.uiPannel.addChild(actBtn3);
			actBtn3.on(Event.CLICK,this,clickHandle);
			
			var actBtn4:Sprite=new Sprite();
			actBtn4.name="10000203";
			actBtn4.loadImage("res/game/lightningBtn.png");
			actBtn4.pos(actBtn1.x+actBtn4.width/2,actBtn1.y-actBtn4.height-15);
			Main.uiPannel.addChild(actBtn4);
			actBtn4.on(Event.CLICK,this,clickHandle);
		}
		//发射子弹
		private function clickHandle(e:Event):void
		{
			// TODO Auto Generated method stub
			if(manualControlMask.parent){
				removeChild(manualControlMask);
				if(boomArea.parent)removeChild(boomArea);
				return;
			}
			manualControl(e.currentTarget,e.currentTarget.name);
			return;
			var actBtn:Sprite=e.currentTarget as Sprite;
			var b:Bullet=new Bullet();
			b.staticId=NumericalValueData.getInstance().PropsData["10000101"]["bulletId"];
			b.initData(); 
			addChild(b);
			Util.grayingApe(actBtn);
			actBtn.mouseEnabled=false;
//			var s:Number=Math.PI/180*(360-player.rotation+180);	
//			b.pos(player.x+50*Math.sin(s),player.y+50*Math.cos(s));
//			b.rotation=player.rotation;
			b.emission()
		}
		private function cheakHit():void{
			if(joystick.curData["degrees"]!=joystick.lastData["degrees"]
				||joystick.curData["proportionX"]!=joystick.lastData["proportionX"]
				||joystick.curData["proportionY"]!=joystick.lastData["proportionY"]
			){
				joystick.lastData["degrees"]=joystick.curData["degrees"];
				joystick.lastData["proportionX"]=joystick.curData["proportionX"];
				joystick.lastData["proportionY"]=joystick.curData["proportionY"];
				SocketClient.getInstance().sendData(NumericalValueData.getInstance().apiConfig["ON_SET_TARGET"]);
			}
			if(!playerList)return;
			proofreaderPosition();
			if(SocketClient.getInstance().socketSlow)return;
			
//			for(var i=0;i<playerList.length;i++){
//				var temp:BasicPlayer=playerList[i] as BasicPlayer;
//				if(temp.parent){
//					if(!cheakHitMap(temp)){
//						if(!hitHandle(temp)){
//							temp.speedX=temp._speedX;
//							temp.speedY=temp._speedY;
//							temp.move();
//						}else{
//							temp.stand();
//						}
//					}else{
//						temp.stand();
//					}
//					
//				}
//			}
//			if(!cheakHitMap(player)){
//				if(!hitHandle(player)){
//					player.speedX=player._speedX;
//					player.speedY=player._speedY;
//					player.move();
//				}else{
//					player.stand();
//				}
//			}else{
////				trace("hitBlock")
//				player.stand();
//			}
			
			if(manualControlMask.parent){
				manualControlMask.pos(player.x,player.y);
//				boomArea.x+=player.speedX;
//				boomArea.y+=player.speedY;
			}
			moveMap();
		}
		private function cheakHitMap(_temp:BasicPlayer):Boolean{
			for(var i:Number=0,len=SocketData.getInstance().blockPhoto.length;i<len;i++){
				var temap:Object=SocketData.getInstance().blockPhoto[i];
				var width:Number=NumericalValueData.getInstance().block[temap.staticId]["boxSize"][0];
				var height:Number=NumericalValueData.getInstance().block[temap.staticId]["boxSize"][1];
//				position
				var recTangle:Rectangle=new Rectangle(temap.position.x,temap.position.y,width,height);
				if(Util.getDistance(_temp.x+_temp.speedX,_temp.y+_temp.speedY,recTangle.x,recTangle.y)<=_temp.hitBox.width+recTangle.width){
//					trace("true")
					_temp.stand()
					return true;
				}
//				if(Util.getDistance(player.x+player.speedX,player.y+player.speedY,recTangle.x,recTangle.y)<=_temp.hitBox.width+recTangle.width){
//					trace("true")
//					return true;
//				}
			}
//			_temp.speedX=_temp._speedX;
//			_temp.speedY=_temp._speedY;
//			_temp.move();
			return false;
		}
		
		private function hitHandle(_temp:BasicPlayer):Boolean{
			for(var j=0;j<playerList.length;j++){
				var otherPlayer:BasicPlayer=playerList[j] as BasicPlayer;
					if(_temp.objectId!=otherPlayer.objectId){
						if(Util.getDistance(_temp.x+_temp.speedX,_temp.y+_temp.speedY,otherPlayer.x,otherPlayer.y)<=_temp.hitBox.width){
							
							return true;
						}
					}
			}
			return false;
		}
		
		/**
		 *校准所有坦克数据 
		 */		
		private function proofreaderPosition():void{
			pingTxt.text=SocketData.getInstance().pingStr;
			timeIndex++;
			if(!SocketData.getInstance().isProofForALL){
				SocketData.getInstance().isProofForALL=true;
				timeIndex=0;
				//玩家位置校正
				player.x=SocketData.getInstance().player.position.x;
				player.y=SocketData.getInstance().player.position.y;
//				player.upDataBlood();
				moveMap();
//				for each(var enemy:Object in SocketData.getInstance().enemyPhoto){
//					var tmpPlayer:Number = ExistsPlayer(Number(enemy["objectId"]));
////					TracePlayerList();
//					if (playerList[tmpPlayer].objectId != 0){
//						playerList[tmpPlayer].x=enemy["position"]["x"]; 
//						playerList[tmpPlayer].y=enemy["position"]["y"];
//						playerList[tmpPlayer].tankBase.rotation=Number(enemy["rotation"]);
////						playerList[tmpPlayer]._speedX=enemy["proportion"]["x"]*playerList[tmpPlayer].maxSpeedX;
////						playerList[tmpPlayer]._speedY=enemy["proportion"]["y"]*playerList[tmpPlayer].maxSpeedY;
////						playerList[tmpPlayer].upDataBlood();
//						this.addChild(playerList[tmpPlayer]);
//					} 
//				}
			}
			SocketData.getInstance().isProofForALL=false;
			
		}
		/**
		 *手动释放技能 
		 * @param _propId 技能ID
		 * 
		 */		
		private function  manualControl(_btn:Sprite,_propId:String="10000201"):void{
			manualControlMask.pos(player.x,player.y);
			var bulletId:String=NumericalValueData.getInstance().PropsData[""+_propId]["bulletId"][0];
			if(NumericalValueData.getInstance().PropsData[""+_propId]["propType"]==1){
				//爆炸盒表
				var data:*={"packetId":NumericalValueData.getInstance().apiConfig["ON_USE_PROPS"],"propsId":_propId,"position":{x:player.x,y:player.y}};
				SocketClient.getInstance().sendData(NumericalValueData.getInstance().apiConfig["ON_USE_PROPS"],data);
				var cd:CDshape=new CDshape();
				cd.cdTime=NumericalValueData.getInstance().PropsData["10000101"]["cd"];
				cd.createCD(_btn);
				cd.pos(_btn.x,_btn.y);
				Main.uiPannel.addChild(cd);
				_btn.mouseEnabled=false;
				Util.grayingApe(_btn);
			}else if(NumericalValueData.getInstance().PropsData[""+_propId]["propType"]==2){
				//走爆飞行器表
				var bulletFlyDistance:Number=NumericalValueData.getInstance().bulletData[bulletId]["bulletFlyDistance"]*2;
				//			var boxSize:Number=NumericalValueData.getInstance().bulletData[bulletId]["boxSize"]*2;
				var boxSize:Number=100;
				manualControlMask.scaleX=manualControlMask.scaleY=bulletFlyDistance/manualControlMask.width;
				boomArea.scaleX=boomArea.scaleY=boxSize/boomArea.width;
				this.addChild(manualControlMask);
				this.addChild(player);
				this.addChild(boomArea);
				manualControlMask.on(Event.MOUSE_DOWN,this,manualControlDown,[_propId]);
			}
		}
		
		private function manualControlDown(_propId:String,e:Event):void
		{
			// TODO Auto Generated method stub
			e.stopPropagation();
			var clickVector:Vector2=new Vector2(e.stageX-this.x,e.stageY-this.y);
			boomArea.pos(clickVector.x,clickVector.y);
			this.on(Event.MOUSE_MOVE,this,manualControlMove,[_propId]);
			this.on(Event.MOUSE_UP,this,manualControlUp,[_propId]);
		}
		
		private function manualControlMove(_propId:String,e:Event):void
		{
			// TODO Auto Generated method stub
			e.stopPropagation();
//			var clickVector:Vector2=new Vector2(e.stageX-this.x,e.stageY-this.y);
			var   np:Point=new Point(e.stageX-this.x,e.stageY-this.y);
			var s:Number = Util.getDistance(manualControlMask.x,manualControlMask.y,np.x,np.y);
			var r:Number=(manualControlMask.width*manualControlMask.scaleX)/2;
			var radian:Number = Math.floor(Math.atan2((np.y-manualControlMask.y),(np.x-manualControlMask.x))*10000)/10000;
			boomArea.x = s>r?Math.cos(radian)*r+manualControlMask.x:np.x;
			boomArea.y = s>r?Math.sin(radian)*r+manualControlMask.y:np.y;
		}
		
		private function cleanManualControl():void{
			removeChild(manualControlMask);
			manualControlMask.off(Event.MOUSE_DOWN,this,manualControlDown);
			this.off(Event.MOUSE_MOVE,this,manualControlMove);
			this.off(Event.MOUSE_UP,this,manualControlUp);
			Tween.to(boomArea, { alpha :0  }, 400);
			Tween.to(boomArea, { alpha :1  },200,null,null,400);
			Tween.to(boomArea, { alpha :0  }, 500,null,new Handler(this,manualControlMaskHandle),800);
		}
		
		private function manualControlMaskHandle():void{
			boomArea.alpha=1;
			removeChild(boomArea);
		}
		
		private function manualControlUp(_propId:String,e:Event):void
		{
			// TODO Auto Generated method stub
			e.stopPropagation();
			cleanManualControl();
			var radian:Number=Math.floor(Math.atan2(boomArea.y-player.y,boomArea.x-player.x)*10000)/10000;
			player.tankBase.rotation=Math.floor(radian*180/Math.PI);
			var data:*={"packetId":NumericalValueData.getInstance().apiConfig["ON_USE_PROPS"],"propsId":_propId,"position":{x:boomArea.x,y:boomArea.y}};
			SocketClient.getInstance().sendData(NumericalValueData.getInstance().apiConfig["ON_USE_PROPS"],data);
//			SocketClient.getInstance().socket.send(JSON.stringify({"packetId":106,"propsId":"10000203","position":{x:boomArea.x,y:boomArea.y}}));
//			var b:Bullet=new Bullet();
//			b.staticId=NumericalValueData.getInstance().PropsData["10000201"]["bulletId"];
//			b.initData(); 
//			b.maxDistance=Util.getDistance(player.x,player.y,boomArea.x,boomArea.y);
//			b.targetPoint.x=boomArea.x;
//			b.targetPoint.y=boomArea.y;
//			b.x=player.x;
//			b.y=player.y;
//			b.origin.x=player.x;
//			b.origin.y=player.y;
//			addChild(b);
//			var s:Number=Math.PI/180*(360-player.rotation+180);	
////			b.pos(player.x+50*Math.sin(s),player.y+50*Math.cos(s));
////			b.rotation=player.rotation;
//			b.emission()
		}
		
		private function ExistsBullet(objectId:Number):Number{
			var bulletIndex:Number =-1;
			var logContent : String = "ExistsPlayer_ [";
			for(var i:Number=0;i<bulletList.length;i++){
				logContent += "  Id: " + bulletList[i].objectId;
				if(bulletList[i].objectId==objectId){
					bulletIndex=i;
					break;
				}
//				else if(bulletList[i].objectId==0&&bulletIndex==-1){
//					bulletIndex=i;
//				}                  
			}
//			logContent += "]  objectId: " + objectId +" result:" + bulletIndex + "  playerListLength:" + playerList.length;
			return bulletIndex;
		}
		/**
		 * 判断敌人是否已经创建了
		 * 返回结果： null： 不存在   否则返回对象 
		 */
		private function ExistsPlayer(objectId:Number):Number{
			var playerZero:Number =-1;
			var logContent : String = "ExistsPlayer_ [";
			
			for(var i:Number=0;i<playerList.length;i++){
				logContent += "  Id: " + playerList[i].objectId;
				if(playerList[i].objectId==objectId){
					playerZero=i;
					break;
				}
				else if(playerList[i].objectId==0&&playerZero==-1){
					playerZero=i;
				}
			}
			logContent += "]  objectId: " + objectId +" result:" + playerZero + "  playerListLength:" + playerList.length;
//			trace(logContent)
			return playerZero;
		}
		
		/**
		 * 判断NonVisible是否存在
		 * 返回结果： false： 不存在   否则返回true
		 */ 
		private function ExistsNonVisible(objectId:Number):Boolean{
			var TempResult : Boolean = false;
			for each(var NonVisible:Number in SocketData.getInstance().nonVisible){
				if(NonVisible == objectId){
					TempResult = true;
					break;		
				}
			}
			return TempResult;
		}
		
		/**
		 * 判断敌人是否已经创建了
		 * 返回结果： null： 不存在   否则返回对象 
		 */
		private function TracePlayerList(): void{
			var logContent : String = "";
			for each(var tmpPlayer:BasicPlayer in playerList){
				if(tmpPlayer.parent){
					logContent += "  PlayerObjectId: " + tmpPlayer.objectId;	
				}
			}
//			trace("PlayList Log_" + logContent);
		} 
		/**
		 *  
		 * 清除销毁的子弹 （服务端和本地玩家对象池的差）
		 * 
		 */		
		 private function ClearNoExistBullet():void{
			 for each(var tmpBullet:Bullet in bulletList){
				 if(!ExistsBulletObject(tmpBullet.objectId) && tmpBullet.objectId != 0){
//					 trace(tmpBullet.objectId);
					 var tmpExplode:Object=explodeBullet(tmpBullet.objectId)
					 if(tmpExplode){
//						 this.addChild(EffManager.getInstance().effShow(EffManager.EXPLODED_BULLET,tmpExplode));
						 Main.effPannel.addChild(EffManager.getInstance().effShow(EffManager.EXPLODED_BULLET,tmpExplode));
					 }
					 tmpBullet.clearBullet();
					 tmpBullet.visible=false;
//					 removeChild(tmpBullet);
//					 Pool.recover("bullet",Bullet);
					 bulletList.splice(ExistsBullet(tmpBullet.objectId),1);
				 }
//				if(!ExistsBulletObject(tmpBullet.objectId) && tmpBullet.objectId != 0){
////			 		 trace(tmpBullet.objectId);
//					 var tmpExplode:Object=explodeBullet(tmpBullet.objectId)
//					 if(tmpExplode){
//						 this.addChild(EffManager.getInstance().effShow(EffManager.EXPLODED_BULLET,tmpExplode));
//					 }
//					 tmpBullet.objectId = 0;
//					 tmpBullet.clearBullet();
//					removeChild(tmpBullet);
//				 }
			 }
		 }
		/**
		 *子弹爆炸 
		 * 
		 */	
		 private function explodeBullet(_bulletid:Number):Boolean{
			 var exploded:Object=null;
//			 trace(SocketData.getInstance().explodedPhoto)
			 for each(var tmpBullet:Object in SocketData.getInstance().explodedPhoto){
				 if(tmpBullet.objectId==_bulletid){
					 exploded= tmpBullet;
					 break;
				 }
			 }
			 return exploded;
		 }
		/** 
		 *清除多余的玩家 （服务端和本地玩家对象池的差） 
		 * 
		 */		
		private function ClearNoExistEnemy():void
		{
			for each(var tmpPlayer:BasicPlayer in playerList){
				if(!ExistsEnemy(tmpPlayer.objectId) && tmpPlayer.objectId != 0){
					tmpPlayer.objectId = 0;
				}
			} 
		}
		
		private function ExistsBulletObject(objectId:Number): Boolean{
			var TembResult : Boolean = false;
			for each(var bullet:Object in SocketData.getInstance().bulletPhoto){
				if(Number(bullet["objectId"])==objectId){
					TembResult = true;
					break;
				}
			}
			return TembResult;
		}
		
		private function ExistsEnemy(objectId:Number): Boolean{
			var TempResult : Boolean = false;
			for each(var enemy:Object in SocketData.getInstance().enemyPhoto){
				if(Number(enemy["objectId"])==objectId){
					TempResult = true;
					break;
				}
			}
			return TempResult;
		}
		
	}
}