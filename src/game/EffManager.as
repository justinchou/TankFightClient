package game
{
//	import flash.events.Event;
	import laya.display.Animation;
	import laya.display.Sprite;
	import laya.events.Event;
	import laya.utils.Pool;
	
	public class EffManager extends Sprite
	{
		public static const EXPLODED_BULLET:int=1;
		public var effObj:Sprite=new Sprite();
		
		public var effList:Array=[];
		private static  var _instance:EffManager;
		public function EffManager()
		{
			super();
			if (_instance) {  
				throw new Error("只能用getInstance()来获取实例");  
			}
		}
		
		public static function getInstance():EffManager {
			if(!_instance){
				_instance=new EffManager();
			}
			return _instance;  
		}
		
		public function effShow(_type:Number,_data:*=null):Animation{
			var effSprite:Animation;
			switch(_type)
			{
				case EffManager.EXPLODED_BULLET:
				{
					effSprite=explodedBulletEff(_data);
					break;
				}
					
			}
			return effSprite; 
		}
		
		
		private function explodedBulletEff(_data:*):Animation{
//			var eff:Sprite=new Sprite();
////			eff.graphics.clear();
//			eff.graphics.drawCircle(0,0,20,"#ff0000");
//			eff.x=_data["position"]["x"];
//			eff.y=_data["position"]["y"];
//			effList.push(eff);
//			Tween.to(eff,{alpha:0},2000,null,new Handler(this,clearEff));
			var eff:Animation=Pool.getItemByClass("eff",Animation);
			eff.loadAtlas("res/eff/hit.json");
			eff.pivot(41,46);
			eff.x=_data["position"]["x"];
			eff.y=_data["position"]["y"];
			eff.interval = 50;					// 设置播放间隔（单位：毫秒）
			eff.index = 0;						// 当前播放索引
			eff.play();	
			eff.on(Event.COMPLETE,this,clearEff);
			effList.push(eff);
//			Laya.timer.once(300,this,clearEff,[eff]);
			return eff;
		}
		
		private function clearEff(_eff:Sprite):void{
			effList[0].parent.removeChild(effList[0]);
			effList.shift();
//			trace(effList.length)
		}
	}
}