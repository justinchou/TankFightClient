package game
{
	import data.NumericalValueData;
	
	import laya.display.Sprite;
	import laya.utils.Pool;
	
	public class MapPannel extends Sprite
	{
		private var mapList:Array;
		public function MapPannel()
		{
			super();
//			for(var i=0;i<NumericalValueData.getInstance().mapConfig["map"]["height"];i++){
//				for(var j=0;j<NumericalValueData.getInstance().mapConfig["map"]["width"];j++){
//					var mapBit:Sprite=new Sprite();
//					mapBit.graphics.drawRect(0,0,150,150,"#232628","#000000",3);
//					addChild(mapBit);
//					mapBit.x=j*150;
//					mapBit.y=i*150;
//				}
//			}
			
			for(var i=0;i<2;i++){
				for(var j=0;j<2;j++){
					var mapBit:Sprite=Pool.getItemByClass("mapBg",Sprite);
					mapBit.loadImage("res/game/mapbg.jpg");
					addChild(mapBit);
					mapBit.x=j*mapBit.width;
					mapBit.y=i*mapBit.height;
				}
			}
			mapList=NumericalValueData.getInstance().map_01["mapList"];
			createMapItem();
//			this.cacheAs="renderTarget";
			cacheAs="bitmap";
		}
		private function createMapItem():void{
			for(var i:Number=0,len=mapList.length;i<len;i++){
				var arr:Array=new Array();
				arr=mapList[i]
				for(var j:Number=0,alen=arr.length;j<alen;j++){
					if(arr[j].length<=1)continue;
					var str:String=arr[j];
					var temArr:Array=str.split("/");
					var mapC:Sprite=Pool.getItemByClass("mapItem",Sprite);
					var mapItem:Sprite=Pool.getItemByClass("mapItem",Sprite);
					mapItem.loadImage("map/"+temArr[0]+".png");
					mapItem.pivot(mapItem.width/2,mapItem.height/2);
					mapC.addChild(mapItem);
					mapItem.rotation =Number(temArr[1])*90;
					mapItem.pos(mapItem.width/2,mapItem.height/2); 
					mapC.cacheAs="bitmap";
					if(temArr[2]=="1"){
						mapItem.scaleX=-1;
//						mapItem.pivotX=-mapItem.width;
					}
					
					mapC.x=21*j;
					mapC.y=21*i+i;
					if(temArr[3]=="1"){
						Main.effPannel.addChild(mapC);
					}else{
						addChild(mapC);
					}
				}
			}
		}
	}
}