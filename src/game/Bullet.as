package game
{
	import data.NumericalValueData;
	
	import laya.d3.math.Vector2;
	import laya.display.Sprite;
	import laya.maths.Point;
	
	import util.Util;
	
	
	public class Bullet extends Sprite
	{
		
		public var objectId:Number=0;
		public var staticId:Number=0;
		//目标类型 1：自身  2：友军  3：敌军  4：全体
		public var targetType:Number;
		//穿透等级  -1：不穿透  1：穿透，过程伤害  2：穿透，过程无伤
		public var penetratetType:Number;
		//发射角度
		public var emitRotion:Number;
		//碰撞盒起点
		public var hitBoxP:Point;
		//碰撞盒类型
		public var hitBoxType:Number;
		//碰撞盒大小
		public var hitBoxSize:Array;
		//发射间隔
		public var bulletDelay:Number;
		//飞行距离
		public var maxDistance:Number;
		public var maxSpeed:Number;
		
		private var speedX:Number=0;
		private var speedY:Number=0;
		//起始位置
		public var origin:Point=new Point();
		
		//目标点
		public var targetPoint:Point=new Point();

		private var vectorS:Vector2;
		
		
		private var lastTime:Number=0;
		public function Bullet()
		{
			super();
			this.loadImage("res/game/bullet.png");  
			this.pivot(this.width/2,0);
		}
		
		public function initData():void
		{
			// TODO Auto Generated method stub
			targetType=NumericalValueData.getInstance().bulletData[staticId+""]["targetType"];	
			penetratetType=NumericalValueData.getInstance().bulletData[staticId+""]["through"];
			emitRotion=NumericalValueData.getInstance().bulletData[staticId+""]["rotationOffset"];
			hitBoxP=NumericalValueData.getInstance().bulletData[staticId+""]["positionOffset"];
			hitBoxType=NumericalValueData.getInstance().bulletData[staticId+""]["boxType"];
			hitBoxSize=NumericalValueData.getInstance().bulletData[staticId+""]["boxSize"];
			bulletDelay=NumericalValueData.getInstance().bulletData[staticId+""]["delay"];
			maxDistance=NumericalValueData.getInstance().bulletData[staticId+""]["bulletFlyDistance"];
//			maxSpeed=NumericalValueData.getInstance().bulletData[staticId+""]["bulletFlySpeed"];
			maxSpeed=120;
		}
		public function emission():void{
//			origin=new Point(_x,_y);
//			var radian:Number=Math.atan2(targetPoint.y-origin.y,targetPoint.x-origin.x);
//			radian=Math.floor(radian*10000)/10000;
//			this.speedX=Math.floor((this.maxSpeed/20)*Math.cos(radian)*10000)/10000;
//			this.speedY=Math.floor((this.maxSpeed/20)*Math.sin(radian)*10000)/10000;
//			trace(Math.sqrt(Math.pow(this.speedX,2)+Math.pow(this.speedY,2)));
//			this.rotation=Math.floor(radian*180/Math.PI)+90;
			lastTime=new Date().getTime();
			var scale:Number = maxSpeed / maxDistance / 1e3;
			vectorS=new Vector2(
				(targetPoint.x-origin.x) * scale,
				(targetPoint.y-origin.y) * scale
			);
//			if(scale==0){
//				trace("targetPoint:"+targetPoint.x,"origin:"+origin.x,"scale:"+scale);
//			}
//			Laya.timer.loop(50,this,move);
		}
		public function move():void{
//			var s:Number=Math.PI/180*(360-this.rotation+180); n 
			var now:Number=new Date().getTime();
			var multipleSpeed:Number=now-lastTime;
			lastTime=now;
			this.pos(this.x+(vectorS.x*multipleSpeed),this.y+(vectorS.y*multipleSpeed));
//			if(this.y>origin.y+320){
//				clearBullet();
//			}
//			if(Util.getDistance(this.x+vectorS.x,this.y+vectorS.y,origin.x,origin.y)>=maxDistance){
//				clearBullet();
//			}
		}
		
		public function clearBullet():void{
//			if(this.parent){
//				this.parent.removeChild(this);
				Laya.timer.clear(this,move);
//				this.destroy();
//			}
		}
		
		public function createBullet():void{
			
		}
	}
}