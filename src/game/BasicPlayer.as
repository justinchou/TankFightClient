package game
{
	import data.NumericalValueData;
	import data.SocketData;
	
	import event.GameEvent;
	
	import laya.display.Sprite;
	import laya.events.Event;
	import laya.utils.Handler;
	import laya.utils.Tween;
	
	import util.Util;
	
	public class BasicPlayer extends Sprite
	{
		public var maxSpeedX:Number=75/20; 
		public var maxSpeedY:Number=75/20;
		
		public var speedX:Number=0;
		public var speedY:Number=0;
		public var _speedX:Number=0;
		public var _speedY:Number=0;
		public var hitId:Number=1;
		public var hitBox:Sprite;
		public var tagetBullet:Bullet;
		public var lastX:Number=0;
		public var lastY:Number=0;
		public var objectId:Number=0;
		//tank底座
		public var tankBase:Sprite;
		//炮管底座
		public var gunBase:Sprite;
		//tank炮管
		public var gun:Sprite;
		
		//玩家状态  0：初始化的玩家  1：在视野内的玩家   2：视野外的玩家
		public var type:Number=0;
		public var HP:Number=0;
		public var maxHp:Number=270;
		public var bloodBg:Sprite;
		public var blood:Sprite;
		public function BasicPlayer(_playerType:Number=1)
		{
			super();
			tankBase=new Sprite();
			this.scaleX=this.scaleY=.5;
			tankBase.loadImage("res/game/tank"+_playerType+".png");
//			tankBase.pivot(Number(NumericalValueData.getInstance().TankData["11900001"]["boxSize"][0]),Number(NumericalValueData.getInstance().TankData["11900001"]["boxSize"][0]));
			tankBase.pivot(tankBase.width/2,tankBase.height/2);
			tankBase.pos(tankBase.pivotX,tankBase.pivotY);
//			tankBase.
			gunBase=new Sprite();
			gunBase.loadImage("res/game/gunBase"+_playerType+".png");
			gunBase.pivot(gunBase.width/3,gunBase.height/2);
			gunBase.pos(tankBase.pivotX,tankBase.pivotY);
			gun=new Sprite();
			gun.loadImage("res/game/gun"+_playerType+".png");
			hitBox=new Sprite();
//			if(NumericalValueData.getInstance().getData(5,hitId,1)[0]>0){
//				hitBox.graphics.drawRect(0,0,Number(NumericalValueData.getInstance().getData(5,hitId,2)[0]),Number(NumericalValueData.getInstance().getData(5,hitId,2)[1]),"#ffffff","#ff0000",4);
//				hitBox.width=Number(NumericalValueData.getInstance().getData(5,hitId,2)[0]);
//				hitBox.height=Number(NumericalValueData.getInstance().getData(5,hitId,2)[1]);
//				this.pivot(Number(NumericalValueData.getInstance().getData(5,hitId,2)[0]/2),Number(NumericalValueData.getInstance().getData(5,hitId,2)[1]/2));
//			}else{
			this.pivot(tankBase.pivotX,tankBase.pivotY);
			hitBox.graphics.drawCircle(tankBase.pivotX,tankBase.pivotX,Number(NumericalValueData.getInstance().TankData["11900001"]["boxSize"][0])/this.scaleX,"#ffffff","#ff0000",4);
			hitBox.width=(Number(NumericalValueData.getInstance().TankData["11900001"]["boxSize"][0])/this.scaleX)*2;
			hitBox.height=(Number(NumericalValueData.getInstance().TankData["11900001"]["boxSize"][0])/this.scaleX)*2;
//					}
			hitBox.alpha=0.3;
			addChild(tankBase);
			addChild(gunBase);
			gunBase.addChild(gun);
			bloodBg=new Sprite();
			bloodBg.loadImage("res/game/bloodBg.png");
			bloodBg.pos(-10,tankBase.height);
			blood=new Sprite();
			blood.loadImage("res/game/blood"+_playerType+".png");
			blood.pos(bloodBg.x+10,bloodBg.y+(bloodBg.height-blood.height)/2);
			addChild(bloodBg);
			addChild(blood);
			blood.scaleX=12;
//			addChild(hitBox);
			this.cacheAs="bitmap";
		}
		
		public function upDataBlood():void{
			var balance:Number=HP/maxHp;
			blood.scaleX=balance*12;
		}
		//清理对象
		public function kill():void{
			Laya.timer.clear(this,move);
		}
		private var last:Number=new Date().getTime();
		public function move():void{
			var now:Number=new Date().getTime();
//			trace(now-last);
			last=now;
			this.x+=Util.floorNumber(speedX);
			this.y+=Util.floorNumber(speedY);
			lastX=this.x;
			lastY=this.y;
			
		}
		public function stand():void{
			speedX=0;
			speedY=0;
			_speedX=0;
			_speedY=0;
		}
		
		public function whirlGun(_degrees:Number):void{
//			var degree: Number = Math.abs(_degrees);
//			var degreeCompletment: Number = 360 - degree;
//			var min: Number = Math.min(degree, degreeCompletment);
//			
//			if (min == degreeCompletment) {
//				min = _degrees > 0 ? -1 * degreeCompletment : degreeCompletment;
//				min -= _degrees;
//			} else min = _degrees;
//			
//			trace("Rotation Is " + min);
//			Tween.to(gunBase, {"rotation" :min}, 100);
//			
//			var degree: Number = Math.abs(_degrees);
//			var degreeCompletment: Number = 360 - degree;
//			var min: Number = Math.min(degree, degreeCompletment);
		
//			
//			if (min == degreeCompletment) {
//				min = _degrees > 0 ? -1 * degreeCompletment : degreeCompletment;
//				min -= _degrees;
//			} else min = _degrees;
//			
//			if (Math.abs(min) > 180 && objectId == SocketData.getInstance().player.objectId)
//				trace("Rotation Is " + _degrees);
			var time:Number=Math.abs(_degrees);
			if ((this.rotation >= 0 && _degrees >= 0) || 
				(this.rotation <= 0 && _degrees <= 0) || 
				(this.rotation <= -90 && _degrees <= 90) || 
				(this.rotation <= 90 && _degrees <= -90)
			) {
				// Rotate Directly
				Tween.to(gunBase, {"rotation": _degrees}, time);
			} else {
				// 
				if(this.rotation>=0) {
					var t1:Number = 180 - time;
					Tween.to(gunBase, {"rotation": -180}, t1);
					this.rotation = 180;
					var t2:Number = time - t1;
					Tween.to(gunBase, {"rotation": _degrees}, t2,null,null,t1);
				} else{
					var t1:Number = 180 - time;
					Tween.to(gunBase, {"rotation": 180}, t1);
					this.rotation = -180;
					var t2:Number = time - t1;
					Tween.to(gunBase, {"rotation": _degrees}, t2,null,Handler.create(this,rotationComplate),t1);
				}
			}
			
			
		}
		private function rotationComplate():void{
			event(GameEvent.GUN_BASE_ROTION_COMPLATE);
		}
	}
} 