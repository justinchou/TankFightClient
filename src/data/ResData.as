package data
{
	import laya.net.Loader;

	public class ResData
	{
		//ui资源
		public static var uiData:Array=[];
		//游戏资源
		public static var gameData:Array=[
			//通信协议表
			{url: "res/data/APIStaticConfig.json", type: Loader.JSON},
			{url: "res/data/webLocation.json", type: Loader.JSON},
			//游戏配置表
			{url: "res/data/Datas/Buff.json", type: Loader.JSON},
			{url: "res/data/Datas/Bullet.json", type: Loader.JSON},
			{url: "res/data/Datas/Damage.json", type: Loader.JSON},
			{url: "res/data/Datas/Drop.json", type: Loader.JSON},
			{url: "res/data/Datas/Props.json", type: Loader.JSON},
			{url: "res/data/Datas/SkillLevel.json", type: Loader.JSON},
			{url: "res/data/Datas/Tank.json", type: Loader.JSON},
			{url: "res/data/MapConfig.json", type: Loader.JSON},
			{url: "res/data/Datas/map_01.json", type: Loader.JSON},
			{url: "res/data/Datas/Block.json", type: Loader.JSON},
			//游戏UI
			{url: "res/game/joystick.png", type: Loader.IMAGE},
			{url: "res/game/joystickBg.png", type: Loader.IMAGE},
			{url: "res/game/tank1.png", type: Loader.IMAGE},
			{url: "res/game/tank2.png", type: Loader.IMAGE},
			{url: "res/game/bulletBtn.png", type: Loader.IMAGE},
			{url: "res/game/lightningBtn.png", type: Loader.IMAGE},
			{url: "res/game/primerBtn.png", type: Loader.IMAGE},
			{url: "res/game/excuseBtn.png", type: Loader.IMAGE},
			{url: "res/game/bloodBg.png", type: Loader.IMAGE},
			{url: "res/game/blood1.png", type: Loader.IMAGE},
			{url: "res/game/blood2.png", type: Loader.IMAGE},
			
			{url: "res/game/bullet.png", type: Loader.IMAGE},
			{url: "res/game/boomArea.png", type: Loader.IMAGE},
			{url: "res/game/releaseArea1.png", type: Loader.IMAGE},
			{url: "res/game/releaseArea2.png", type: Loader.IMAGE},
			{url: "res/game/gun1.png", type: Loader.IMAGE},
			{url: "res/game/gunBase1.png", type: Loader.IMAGE},
			{url: "res/game/gun2.png", type: Loader.IMAGE},
			{url: "res/game/gunBase2.png", type: Loader.IMAGE},
			{url: "res/eff/fire.json", type: Loader.ATLAS},
			{url: "res/eff/hit.json", type: Loader.ATLAS},
			{url: "res/game/map.json", type: Loader.ATLAS},
			{url: "res/game/mapbg.jpg", type: Loader.IMAGE},
		];
	}
}