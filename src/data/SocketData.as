package data
{

	public class SocketData
	{
		public var packetId:Number;
		public var rotation:Number=0;
		public var radian:Number=0;
		public var x:Number=0;
		public var y:Number=0;
		public var objID:Number=0;
		public var player:Object={"position":{"x":100,"y":100},"objectId":10,"rotation":0,"radian":0};
		public var enemy:Array=[{"position":{"x":0,"y":0},"objectId":0,"rotation":0,"radian":0,"proportion":{"x":0,"y":0}}];
		public var exploded:Array=[{"objectId":0,"staticId":0,"position":{"x":0,"y":0}}];
		public var bullet:Array=[];
		public var isProcessorForEnemy:Boolean=true;
		public var isProofForALL:Boolean=false;
		public var enemyPhoto:Array=[];
		public var bulletPhoto:Array=[];
		public var explodedPhoto:Array=[];
		public var block:Array=[];
		public var blockPhoto:Array=[];
//		public var allPlayer:Array=[{"objectId":9,"x":100,"y":100,"rotation":0,"radian":0}];
		public var dead:Array=[];
		public var nonVisible:Array=[];
		public var pingStr:String="";
		private static  var _instance:SocketData;
		public function SocketData()
		{
			if (_instance) {  
				throw new Error("只能用getInstance()来获取实例");  
			}
		}
		
		public function setData():void{
//			this.packetId=100;
//			this.radian=0.2351;
//			this.rotate=-12;
//			this.x=0.4574;
//			this.y=0.7596;
			
		}
		
		public static function getInstance():SocketData {
			if(!_instance){
				_instance=new SocketData();
			}
			return _instance;  
		}
		
		public function debug(pos):void {
			var enemyIds = [];
			for (var id = 0; id < this.enemy.length; id ++) {
				enemyIds.push(enemy[id].objectId);
			}
			console.log("Debug Enemy " + pos + "  " + enemyIds);
		}
	}
}