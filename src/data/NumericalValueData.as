package data
{
	public class NumericalValueData
	{
		private static  var _instance:NumericalValueData;
		public var TankData:JSON;
		private var targetData:JSON;
		public var PropsData:JSON;
		public var Damage:JSON;
		public var BUFFData:JSON;
		public var bulletData:JSON;
		public var skillLevelData:JSON;
		//通信协议表
		public var apiConfig:JSON;
		//地图配置文件
		public var mapConfig:JSON;
		//服务器地址
		public var webLocation:JSON;
		//地图配置表
		public var map_01:JSON;
		//地图块碰撞盒
		public var block:JSON;
		public function NumericalValueData()
		{
			if (_instance) {  
				throw new Error("只能用getInstance()来获取实例");  
			}
			init()
		}
		
		private function init():void
		{
			// TODO Auto Generated method stub
			TankData=Laya.loader.getRes("res/data/Datas/Tank.json");
			PropsData=Laya.loader.getRes("res/data/Datas/Props.json");
			Damage=Laya.loader.getRes("res/data/Datas/Damage.json");
			BUFFData=Laya.loader.getRes("res/data/Datas/BUFF.json");
			bulletData=Laya.loader.getRes("res/data/Datas/Bullet.json");
			skillLevelData=Laya.loader.getRes("res/data/Datas/SkillLevel.json");
			apiConfig=Laya.loader.getRes("res/data/APIStaticConfig.json");
			webLocation=Laya.loader.getRes("res/data/webLocation.json");
			mapConfig=Laya.loader.getRes("res/data/MapConfig.json");
			map_01=Laya.loader.getRes("res/data/Datas/map_01.json");
			block=Laya.loader.getRes("res/data/Datas/Block.json");
		}
		public static function getInstance():NumericalValueData {
			if(!_instance){
				_instance=new NumericalValueData();
			}
			return _instance;  
		}
		/**
		 * 获取表格信息
		 * @param _dataTyep 1000:物品表  2000：升级属性表 3：伤害积分表  4：全局常量表  5：碰撞盒表   6：排行奖励表  7：技能表   8：NPC表  9：AI表  10:爆炸盒表 11：BUFF表  12：飞行器表  13：技能等级表 
		 * @param _id 查询ID(行)
		 * @param _index 列索引
		 * @return 返回数据信息 array
		 */		
/*		public function getData(_dataTyep:Number,_id:Number,_index:Number):Array{
			switch(_dataTyep)
			{
				case 1:
				{
					
					break;
				}
					case 2:
				{
					
					break;
				}
					case 3:
				{
					
					break;
				}
					case 4:
				{
					
					break;
				}
					case 5:
					{
						targetData=hitBoxData;
						break;
					}
					case 6:
					{
						
						break;
					}
					case 7:
					{
						targetData=skillData;
						break;
					}
					case 8:
					{
						
						break;
					}
					case 9:
					{
						
						break;
					}
					case 10:
					{
						targetData=boomBoxData;
						break;
					}
					case 11:
					{
						targetData=BUFFData;
						break;
					}
					case 12:
					{
						targetData=bulletData;
						break;
					}
					case 13:
					{
						targetData=skillLevelData;
						break;
					}
					
			}
			return cheakData(_id,_index);
		}
		
		private function cheakData(_id:Number,_index:Number):Array{
			if(!targetData){
				return  null;
			}
			if(String(targetData[_id][_index]).indexOf(",")>0){
				return String(targetData[_id][_index]).split(",");
			}else{
				return String(targetData[_id][_index]).split();
			}
			
			
		}*/
	}
}