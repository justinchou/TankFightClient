package event
{
	public class GameEvent
	{
		public static const PLAYER_MOVE:String="PLAYER_MOVE";
		public static const PLAYER_STOP:String="PLAYER_STOP";
		public static const CREATE_OTHER:String="CREATE_OTHER";
		public static const PLAYER_ATTACK:String="PLAYER_ATTACK";
		public static const GAME_START:String="GAME_START";
		public static const SHOP_SHOW:String="SHOP_SHOW";
		public static const GAME_SELECTEDMAP:String="GAME_SELECTEDMAP";
		public static const GAME_SELECTEDPLAYER:String="GAME_SELECTEDPLAYER";
		public static const GAME_RETURNMAIN:String="GAME_RETURNMAIN";
		public static const GAME_OVER:String="GAME_OVER";
		public static const LOWER_BLOOD_PLAYER:String="GAME_SELECTEDPLAYER";
		public static const UPDATE_MAP:String="UPDATE_MAP";
		public static const PLAYER_ID:String="PLAYER_ID";
		
		public static const TO_ADD_PLAYER:String="TO_ADD_PLAYER";
		
		public static const GUN_BASE_ROTION_COMPLATE:String="GUN_BASE_ROTION_COMPLATE";
	}
}