
package util
{
	import laya.display.Sprite;
	import laya.filters.ColorFilter;

	public class Util
	{
		/**
		 * 得到随机数
		 * @param _min 最小值  
		 * @param _max  最大值
		 * @return  随机值
		 * 
		 */		
		public static function randomNum(_min:int,_max:int):int{
			return Math.floor((_max - _min + 1) * Math.random()) + _min;
		}
		/**
		 * 两对象距离方法
		 * @param _ax a对象X值
		 * @param _ay a对象Y值
		 * @param _bx b对象X值
		 * @param _by b对象Y值
		 * @return 
		 * 
		 */		
		public static function getDistance(_ax:Number,_ay:Number,_bx:Number,_by:Number):Number{
			return Math.sqrt(Math.pow(Math.abs(_ax-_bx),2) + Math.pow(Math.abs(_ay-_by),2));
		}
		/**
		 *灰色滤镜 
		 * @param _img 添加滤镜的对象
		 * 
		 */		
		public static function grayingApe(_img:Sprite):void{
			if(!_img)return;
			var grayscaleMat:Array = [
				0.3086, 0.6094, 0.0820, 0, 0, 
				0.3086, 0.6094, 0.0820, 0, 0, 
				0.3086, 0.6094, 0.0820, 0, 0, 
				0, 0, 0, 1, 0];
			
			//创建一个颜色滤镜对象，灰图
			var grayscaleFilter:ColorFilter = new ColorFilter(grayscaleMat);
			_img.filters=[grayscaleFilter];
		}
		/**
		 *清除滤镜
		 * @param _img 添加滤镜的对象
		 * 
		 */		
		public static function cleanGrayingApe(_img:Sprite):void{
			if(!_img)return;
			var grayscaleMat:Array = [];
			
			//创建一个颜色滤镜对象，灰图
			_img.filters=[];
		}
		
		/**
		 *保留4位小数 
		 */		
		public static function floorNumber(_num:Number):Number{
			if(!_num)return 0;
			_num=Math.floor(_num*10000)/10000
			return _num;
		}
	}
}